package com.vonglasow.michael.voltagedrop;

import java.text.DecimalFormat;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class VoltageDropUi extends Activity implements OnFocusChangeListener, OnItemSelectedListener, TextWatcher {
	private DecimalFormat resDf = new DecimalFormat("#.####");
	private DecimalFormat df2 = new DecimalFormat("#.##");
	private TextView currentValue;
	private Spinner gaugeUnit;
	private TextView gaugeValue;
	private Spinner lengthUnit;
	private TextView lengthValue;
	private Spinner phaseCoefficient;
	private Spinner resistancePreset;
	private TextView resistanceValue;
	private TextView result;
	private Spinner voltageDropUnit;
	private TextView voltageDropValue;
	private AutoCompleteTextView voltageValue;

	@Override
	public void afterTextChanged(Editable s) {
		calculate();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// NOP
	}

	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState If the activity is being re-initialized after 
	 * previously being shut down then this Bundle contains the data it most 
	 * recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		currentValue = (TextView) findViewById(R.id.currentValue);
		gaugeUnit = (Spinner) findViewById(R.id.gaugeUnit);
		gaugeValue = (TextView) findViewById(R.id.gaugeValue);
		lengthUnit = (Spinner) findViewById(R.id.lengthUnit);
		lengthValue = (TextView) findViewById(R.id.lengthValue);
		phaseCoefficient = (Spinner) findViewById(R.id.phaseCoefficient);
		resistancePreset = (Spinner) findViewById(R.id.resistancePreset);
		resistanceValue = (TextView) findViewById(R.id.resistanceValue);
		result = (TextView) findViewById(R.id.result);
		voltageDropUnit = (Spinner) findViewById(R.id.voltageDropUnit);
		voltageDropValue = (TextView) findViewById(R.id.voltageDropValue);
		voltageValue = (AutoCompleteTextView) findViewById(R.id.voltageValue);

		initGaugeUnitSpinner();
		initLengthUnitSpinner();
		initPhaseCoefficientSpinner();
		initResistancePresetSpinner();
		initVoltageDropUnitSpinner();
		initVoltageValues();

		for (TextView textView : new TextView[]{currentValue, gaugeValue, lengthValue, resistanceValue,
				voltageDropValue, voltageValue})
			textView.addTextChangedListener(this);
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (hasFocus && v.equals(voltageValue))
			voltageValue.showDropDown();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		if (parent.getId() == R.id.resistancePreset) {
			/*
    		result.setText((CharSequence) parent.getSelectedItem());
			 */
			switch(position) {
			case 0:
				resistanceValue.setText(resDf.format(VoltageDropCore.RHO_COPPER));
				resistanceValue.setEnabled(false);
				break;
			case 1:
				resistanceValue.setText(resDf.format(VoltageDropCore.RHO_ALUMINIUM));
				resistanceValue.setEnabled(false);
				break;
			default:
				resistanceValue.setEnabled(true);
			}
		}
		calculate();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// NOP
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// NOP
	}

	private void calculate() {
		StringBuilder builder = new StringBuilder();
		int empty = 0;
		
		Double voltageDrop = getDoubleFromTextView(voltageDropValue);
		Double current = getDoubleFromTextView(currentValue);
		Double gauge = getDoubleFromTextView(gaugeValue);
		Double length = getDoubleFromTextView(lengthValue);
		// TODO unit conversion for gauge and length if we support non-SI units
		
		for (Double value : new Double[]{voltageDrop, current, gauge, length})
			if (value == null)
				empty++;
		
		if (empty > 1)
			builder.append(this.getString(R.string.result_incomplete));
		else {
			Double resistance = getResistance();
			
			if (resistance == null)
				builder.append(this.getString(R.string.result_resistance_missing));
			else {
				Double voltage = getDoubleFromTextView(voltageValue);
				int pc = (phaseCoefficient.getSelectedItemPosition() == 1) ? 1 : 2;

				if ((voltageDrop == null) || (empty == 0)) {
					double resultVolts = VoltageDropCore.getVoltageDropVolts(length, pc, resistance,
							gauge, null, null, current);
					if (voltage == null)
						builder.append(String.format(this.getString(R.string.result_voltage_drop_volts),
								df2.format(resultVolts)));
					else {
						double resultPercent = VoltageDropCore.getVoltageDropPercent(voltage, length, pc,
								resistance, gauge, null, null, current);
						builder.append(String.format(this.getString(R.string.result_voltage_drop_percent),
								df2.format(resultVolts),
								df2.format(resultPercent)));
					}
				}
				
				if ((voltage == null) && (voltageDropUnit.getSelectedItemPosition() == 0) && (voltageDrop != null))
					builder.append(this.getString(R.string.result_voltage_missing));
				
				if ((current == null) || (empty == 0)) {
					if (voltageDropUnit.getSelectedItemPosition() == 0) {
						if (voltage != null) {
							double resultCurrent = VoltageDropCore.getCurrent(voltageDrop, voltage,
									length, pc, resistance, gauge, null, null);
							builder.append(String.format(this.getString(R.string.result_current),
									df2.format(resultCurrent)));
						}
					} else {
						double resultCurrent = VoltageDropCore.getCurrent(voltageDrop, length, pc,
								resistance, gauge, null, null);
						builder.append(String.format(this.getString(R.string.result_current),
								df2.format(resultCurrent)));
					}
				}
				
				if ((gauge == null) || (empty == 0)) {
					if (voltageDropUnit.getSelectedItemPosition() == 0) {
						if (voltage != null) {
							double resultGauge = VoltageDropCore.getWireGauge(voltageDrop, voltage,
									length, pc, resistance, null, null, current);
							builder.append(String.format(this.getString(R.string.result_gauge),
									df2.format(resultGauge)));
						}
					} else {
						double resultGauge = VoltageDropCore.getWireGauge(voltageDrop, length, pc,
								resistance, null, null, current);
						builder.append(String.format(this.getString(R.string.result_gauge),
								df2.format(resultGauge)));
					}
				}
				
				if ((length == null) || (empty == 0)) {
					if (voltageDropUnit.getSelectedItemPosition() == 0) {
						if (voltage != null) {
							double resultLength = VoltageDropCore.getWireLength(voltageDrop, voltage,
									pc, resistance, gauge, null, null, current);
							builder.append(String.format(this.getString(R.string.result_length),
									df2.format(resultLength)));
						}
					} else {
						double resultLength = VoltageDropCore.getWireLength(voltageDrop, pc, resistance,
								gauge, null, null, current);
						builder.append(String.format(this.getString(R.string.result_length),
								df2.format(resultLength)));
					}
				}
			}
		}
		result.setText(builder.toString());
	}

	private Double getResistance() {
		Double res = null;
		
		switch(resistancePreset.getSelectedItemPosition()) {
		case 0:
			res = VoltageDropCore.RHO_COPPER;
			break;
		case 1:
			res = VoltageDropCore.RHO_ALUMINIUM;
			break;
		case 2:
			if (resistanceValue.getText().length() > 0)
				res = Double.valueOf(resistanceValue.getText().toString()) * VoltageDropCore.RHO_FACTOR_20_DEGREES_CELSIUS;
			break;
		case 3:
			if (resistanceValue.getText().length() > 0)
				res = Double.valueOf(resistanceValue.getText().toString());
			break;
		}
		if ((res != null) && (res == 0))
			res = null;
		return res;
	}

	private Double getDoubleFromTextView(TextView textView) {
		Double res = null;
		
		if (textView.getText().length() > 0)
			res = Double.valueOf(textView.getText().toString());
		if ((res != null) && (res == 0))
			res = null;
		return res;
	}

	private void initGaugeUnitSpinner() {
		ArrayAdapter<CharSequence> gaugeUnitAdapter = ArrayAdapter.createFromResource(this,
				R.array.gauge_units, android.R.layout.simple_spinner_item);
		gaugeUnitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		gaugeUnit.setAdapter(gaugeUnitAdapter);
		gaugeUnit.setOnItemSelectedListener(this);
	}

	private void initLengthUnitSpinner() {
		ArrayAdapter<CharSequence> lengthUnitAdapter = ArrayAdapter.createFromResource(this,
				R.array.length_units, android.R.layout.simple_spinner_item);
		lengthUnitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		lengthUnit.setAdapter(lengthUnitAdapter);
		lengthUnit.setOnItemSelectedListener(this);
	}

	private void initPhaseCoefficientSpinner() {
		ArrayAdapter<CharSequence> phaseCoefficientAdapter = ArrayAdapter.createFromResource(this,
				R.array.phase_coefficients, android.R.layout.simple_spinner_item);
		phaseCoefficientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		phaseCoefficient.setAdapter(phaseCoefficientAdapter);
		phaseCoefficient.setOnItemSelectedListener(this);
	}

	private void initResistancePresetSpinner() {
		ArrayAdapter<CharSequence> resistancePresetAdapter = ArrayAdapter.createFromResource(this,
				R.array.resistance_presets, android.R.layout.simple_spinner_item);
		resistancePresetAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		resistancePreset.setAdapter(resistancePresetAdapter);
		resistancePreset.setOnItemSelectedListener(this);
	}

	private void initVoltageDropUnitSpinner() {
		ArrayAdapter<CharSequence> voltageDropUnitAdapter = ArrayAdapter.createFromResource(this,
				R.array.voltage_drop_units, android.R.layout.simple_spinner_item);
		voltageDropUnitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		voltageDropUnit.setAdapter(voltageDropUnitAdapter);
		voltageDropUnit.setOnItemSelectedListener(this);
	}

	private void initVoltageValues() {
		ArrayAdapter<CharSequence> voltageValueAdapter = ArrayAdapter.createFromResource(this,
				R.array.voltage_values, android.R.layout.simple_dropdown_item_1line);
		voltageValue.setAdapter(voltageValueAdapter);
		voltageValue.setOnItemSelectedListener(this);
		voltageValue.setOnFocusChangeListener(this);
	}
}

