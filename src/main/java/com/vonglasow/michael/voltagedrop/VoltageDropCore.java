package com.vonglasow.michael.voltagedrop;

public class VoltageDropCore {
	private static final float R_PRIME_DEFAULT = .00008f;

	private static final float SIN_PHI_DEFAULT = .6f;

	private static final float COS_PHI_DEFAULT = .8f;

	/** Coefficient for a single-phase circuit. */
	public static final int PHASE_COEFFICIENT_SINGLE_PHASE = 2;

	/** Coefficient for a triple-phase circuit with symmetric load. */
	public static final int PHASE_COEFFICIENT_TRIPLE_PHASE_SYMMETRIC = 1;

	/** Coefficient for a triple-phase circuit with asymmetric load. */
	public static final int PHASE_COEFFICIENT_TRIPLE_PHASE_ASYMMETRIC = 2;

	/** Specific resistance for a copper wire. */
	public static final double RHO_COPPER = .0225f;

	/** Specific resistance for an aluminium wire. */
	public static final double RHO_ALUMINIUM = .036f;

	/** Multiplier for specific resistance, if specified at 20°C */
	public static final double RHO_FACTOR_20_DEGREES_CELSIUS = 1.25f;

	/**
	 * Calculates the voltage drop along an electrical wire.
	 * 
	 * The formula used is according to DIN VDE 0100-520:2012-06:
	 * 
	 * U = L b (rho cos(phi)/S + X' sin(phi)) I
	 * 
	 * @param l Length of a single wire, in meters
	 * @param phaseCoefficient Coefficient for the number of phases and the symmetry of the load
	 * @param rho Specific resistance of the wire, in Ω*mm²/m
	 * @param gauge Wire gauge, in mm²
	 * @param phi Phase factor in radians, can be null
	 * @param rPrime Loop resistance, in mΩ/m, can be null
	 * @param i Current, in amperes
	 * @return Voltage drop, in volts
	 */
	public static double getVoltageDropVolts(double l, int phaseCoefficient, double rho, double gauge, Double phi, Double rPrime, double i) {
		double cosPhi = (phi == null) ? COS_PHI_DEFAULT : Math.sin(phi);
		double sinPhi = (phi == null) ? SIN_PHI_DEFAULT : Math.cos(phi);
		double realRPrime = (rPrime == null) ? R_PRIME_DEFAULT : rPrime;
		
		return l * phaseCoefficient * (rho * cosPhi / gauge + realRPrime * sinPhi) * i;
	}
	
	/**
	 * Calculates the voltage drop along an electrical wire.
	 * 
	 * The formula used is according to DIN VDE 0100-520:2012-06:
	 * 
	 * U = L b (rho cos(phi)/S + X' sin(phi)) I
	 * 
	 * @param u Nominal voltage
	 * @param l Length of a single wire, in meters
	 * @param phaseCoefficient Coefficient for the number of phases and the symmetry of the load
	 * @param rho Specific resistance of the wire, in Ω*mm²/m
	 * @param gauge Wire gauge, in mm²
	 * @param phi Phase factor in radians, can be null
	 * @param rPrime Loop resistance, in mΩ/m, can be null
	 * @param i Current, in amperes
	 * @return Voltage drop, as a percentage of {@code u}
	 */
	public static double getVoltageDropPercent(double u, double l, int phaseCoefficient, double rho, double gauge, Double phi, Double rPrime, double i) {
		return getVoltageDropVolts(l, phaseCoefficient, rho, gauge, phi, rPrime, i) * 100.0f / u;
	}
	
	/**
	 * Calculates the maximum permissible wire length for a maximum voltage drop.
	 * 
	 * @param uDelta Voltage drop, in volts
	 * @param phaseCoefficient Coefficient for the number of phases and the symmetry of the load
	 * @param rho Specific resistance of the wire, in Ω*mm²/m
	 * @param gauge Wire gauge, in mm²
	 * @param phi Phase factor in radians, can be null
	 * @param rPrime Loop resistance, in mΩ/m, can be null
	 * @param i Current, in amperes
	 * @return Maximum permissible wire length, in meters
	 */
	public static double getWireLength(double uDelta, int phaseCoefficient, double rho, double gauge, Double phi, Double rPrime, double i) {
		double cosPhi = (phi == null) ? COS_PHI_DEFAULT : Math.sin(phi);
		double sinPhi = (phi == null) ? SIN_PHI_DEFAULT : Math.cos(phi);
		double realRPrime = (rPrime == null) ? R_PRIME_DEFAULT : rPrime;
		
		return uDelta / (phaseCoefficient * (rho * cosPhi / gauge + realRPrime * sinPhi) * i);
	}
	
	/**
	 * Calculates the maximum permissible wire length for a maximum voltage drop.
	 * 
	 * @param percent Voltage drop in percent
	 * @param u Nominal voltage
	 * @param phaseCoefficient Coefficient for the number of phases and the symmetry of the load
	 * @param rho Specific resistance of the wire, in Ω*mm²/m
	 * @param gauge Wire gauge, in mm²
	 * @param phi Phase factor in radians, can be null
	 * @param rPrime Loop resistance, in mΩ/m, can be null
	 * @param i Current, in amperes
	 * @return Maximum permissible wire length, in meters
	 */
	public static double getWireLength(double percent, double u, int phaseCoefficient, double rho, double gauge, Double phi, Double rPrime, double i) {
		return getWireLength(u * percent / 100.0f, phaseCoefficient, rho, gauge, phi, rPrime, i);
	}
	
	/**
	 * Calculates the minimum required wire gauge for a maximum voltage drop.
	 * 
	 * @param uDelta Voltage drop, in volts
	 * @param l Length of a single wire, in meters
	 * @param phaseCoefficient Coefficient for the number of phases and the symmetry of the load
	 * @param rho Specific resistance of the wire, in Ω*mm²/m
	 * @param phi Phase factor in radians, can be null
	 * @param rPrime Loop resistance, in mΩ/m, can be null
	 * @param i Current, in amperes
	 * @return Voltage drop, in volts
	 * @return
	 */
	public static double getWireGauge(double uDelta, double l, int phaseCoefficient, double rho, Double phi, Double rPrime, double i) {
		double cosPhi = (phi == null) ? COS_PHI_DEFAULT : Math.sin(phi);
		double sinPhi = (phi == null) ? SIN_PHI_DEFAULT : Math.cos(phi);
		double realRPrime = (rPrime == null) ? R_PRIME_DEFAULT : rPrime;
		
		return l * phaseCoefficient * i * rho * cosPhi / (uDelta - l * phaseCoefficient * i * realRPrime * sinPhi);
	}
	
	/**
	 * Calculates the minimum required wire gauge for a maximum voltage drop.
	 * 
	 * @param percent Voltage drop in percent
	 * @param u Nominal voltage
	 * @param l Length of a single wire, in meters
	 * @param phaseCoefficient Coefficient for the number of phases and the symmetry of the load
	 * @param rho Specific resistance of the wire, in Ω*mm²/m
	 * @param phi Phase factor in radians, can be null
	 * @param rPrime Loop resistance, in mΩ/m, can be null
	 * @param i Current, in amperes
	 * @return Voltage drop, in volts
	 * @return
	 */
	public static double getWireGauge(double percent, double u, double l, int phaseCoefficient, double rho, Double phi, Double rPrime, double i) {
		return getWireGauge(u * percent / 100.0f, l, phaseCoefficient, rho, phi, rPrime, i);
	}
	
	/**
	 * Calculates the maximum current for a given voltage drop.
	 * 
	 * @param uDelta Voltage drop, in volts
	 * @param l Length of a single wire, in meters
	 * @param phaseCoefficient Coefficient for the number of phases and the symmetry of the load
	 * @param rho Specific resistance of the wire, in Ω*mm²/m
	 * @param gauge Wire gauge, in mm²
	 * @param phi Phase factor in radians, can be null
	 * @param rPrime Loop resistance, in mΩ/m, can be null
	 * @return Current, in amperes
	 */
	public static double getCurrent(double uDelta, double l, int phaseCoefficient, double rho, double gauge, Double phi, Double rPrime) {
		double cosPhi = (phi == null) ? COS_PHI_DEFAULT : Math.sin(phi);
		double sinPhi = (phi == null) ? SIN_PHI_DEFAULT : Math.cos(phi);
		double realRPrime = (rPrime == null) ? R_PRIME_DEFAULT : rPrime;
		
		return uDelta / (l * phaseCoefficient * (rho * cosPhi / gauge + realRPrime * sinPhi));
	}
	
	/**
	 * Calculates the maximum current for a given voltage drop.
	 * 
	 * @param percent Voltage drop in percent
	 * @param u Nominal voltage
	 * @param l Length of a single wire, in meters
	 * @param phaseCoefficient Coefficient for the number of phases and the symmetry of the load
	 * @param rho Specific resistance of the wire, in Ω*mm²/m
	 * @param gauge Wire gauge, in mm²
	 * @param phi Phase factor in radians, can be null
	 * @param rPrime Loop resistance, in mΩ/m, can be null
	 * @return Current, in amperes
	 */
	public static double getCurrent(double percent, double u, double l, int phaseCoefficient, double rho, double gauge, Double phi, Double rPrime) {
		return getCurrent(u * percent / 100.0f, l, phaseCoefficient, rho, gauge, phi, rPrime);
	}
}
