package com.vonglasow.michael.voltagedrop.test;

import com.vonglasow.michael.voltagedrop.VoltageDropCore;

public class VoltageDropTest {

	public static void main(String[] args) {
		double u = 230;
		double percent = 3.0f;
		double uDelta = u * percent / 100.0f;
		double l = 20.0f;
		int phaseCoefficient = VoltageDropCore.PHASE_COEFFICIENT_SINGLE_PHASE;
		double rho = VoltageDropCore.RHO_COPPER;
		double gauge = 1.5f;
		double i = 16;
		
		System.out.printf("Voltage drop for l=%.2f m, single phase, copper wire, %.1f mm², %.1f A:\n", l, gauge, i);
		System.out.printf("  %.1f V\n", VoltageDropCore.getVoltageDropVolts(l, phaseCoefficient, rho, gauge, null, null, i));
		System.out.printf("  At %.1f V: %.1f %%\n", u, VoltageDropCore.getVoltageDropPercent(u, l, phaseCoefficient, rho, gauge, null, null, i));
		
		System.out.printf("Maximum wire length for %.1f V, voltage drop %.1f %%, single phase, copper wire, %.1f mm², %.1f A:\n", u, percent, gauge, i);
		System.out.printf("  By voltage: l=%.2f m\n", VoltageDropCore.getWireLength(uDelta, phaseCoefficient, rho, gauge, null, null, i));
		System.out.printf("  By percent: l=%.2f m\n", VoltageDropCore.getWireLength(percent, u, phaseCoefficient, rho, gauge, null, null, i));
		
		System.out.printf("Minimum wire gauge for %.1f V, voltage drop %.1f %%, l=%.2f m, single phase, copper wire, %.1f A:\n", u, percent, l, i);
		System.out.printf("  By voltage: %.1f mm²\n", VoltageDropCore.getWireGauge(uDelta, l, phaseCoefficient, rho, null, null, i));
		System.out.printf("  By percent: %.1f mm²\n", VoltageDropCore.getWireGauge(percent, u, l, phaseCoefficient, rho, null, null, i));
		
		System.out.printf("Maximum current for %.1f V, voltage drop %.1f %%, l=%.2f m, single phase, copper wire, %.1f mm²:\n", u, percent, l, i);
		System.out.printf("  By voltage: %.1f A\n", VoltageDropCore.getCurrent(uDelta, l, phaseCoefficient, rho, gauge, null, null));
		System.out.printf("  By percent: %.1f A\n", VoltageDropCore.getCurrent(percent, u, l, phaseCoefficient, rho, gauge, null, null));
	}

}
